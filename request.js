/* eslint-disable linebreak-style */
/* eslint-disable import/prefer-default-export */

export function sendRequest(email, endPoint, btn) {
  btn.setAttribute('disabled', true);
  btn.classList.add('opacity');
  const xhr = new XMLHttpRequest();
  xhr.open('post', endPoint, true);
  xhr.responseType = 'json';
  xhr.setRequestHeader('Content-Type', 'Application/json');
  xhr.onload = () => {
    btn.removeAttribute('disabled');
    btn.classList.remove('opacity');
    const data = xhr.response;
    if (xhr.status == 422) {
      window.alert(JSON.stringify(data));
    } else {
      console.log(data);
    }
  };
  xhr.onerror = (error) => { console.log(error); };
  xhr.send(JSON.stringify({ email }));
}
